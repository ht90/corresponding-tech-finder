import sys, getopt
from gensim.models import Word2Vec


def extract_tag_frequencies():
    file_path = "tags_fequencies"
    f = open(file_path, 'r', encoding="utf8")
    current_line = f.readline()
    max_frequency = int(current_line.replace("\n", ""))
    all_tags = {}
    while True:
        current_line = f.readline()
        if not current_line:
            break
        current_line = current_line.replace("\n", "")
        tag = current_line.split("\t")[0]
        count = int(current_line.split("\t")[1])
        all_tags[tag] = count
    return all_tags, max_frequency


def error_and_exit(exit_code):
    print('find_technology.py -s <source-programming-language> -t <technology> -d <target-programming-language')
    sys.exit(exit_code)


def print_technologies(source_lang, technology, target_lang):
    v = Word2Vec.load("tags_word2vec.model")
    out = v.most_similar([target_lang, technology], [source_lang], 30)
    tags_frequencies, max_frequency = extract_tag_frequencies()
    rank = 1
    for item in out:
        print("Rank: %s \t Technology: %s \t\t Significance: %0.3f \t Stack Overflow Frequency: %s" % (
        rank, item[0], item[1], tags_frequencies[item[0]]))
        rank += 1


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hs:t:d:", ["sourcepl=", "tech=", "targetpl="])
    except getopt.GetoptError:
        error_and_exit(2)
    source_lang = ""
    target_lang = ""
    technology = ""
    if not opts:
        error_and_exit(2)
    for opt, arg in opts:
        if opt == '-h':
            error_and_exit(0)
        elif opt in ("-s", "--sourcepl"):
            source_lang = arg
        elif opt in ("-t", "--tech"):
            technology = arg
        elif opt in ("-d", "--targetpl"):
            target_lang = arg
        else:
            sys.exit(2)
    print_technologies(source_lang,technology,target_lang)

if __name__ == "__main__":
    main(sys.argv[1:])
